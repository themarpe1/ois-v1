import java.util.Scanner;

class Gravitacija {

	static void izpis(double visina, double gravitacijskiPospesek )	{
		System.out.println("Nadmorska visina: " + visina + " in gravitacijski pospesek: " + gravitacijskiPospesek+".");
	}

	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);
		
		double v = sc.nextInt(); // nadmorska visina;
		
		double g = izracun(v);
		izpis(v, g);
		
	}
	
	public static double izracun(double v) { // funkcija za izracun
		
		double c = 6.674*Math.pow(10, -11); // gravitacijska konstanta;
		double m = 5.972*Math.pow(10, 24); // masa zemlje;
		double r = 6.371*Math.pow(10, 6); // polmer zemlje;
		
		double g = c*m/((r+v)*(r+v));
		return g;
		
	}
}
